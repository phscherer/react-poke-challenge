# Pokémon Store

Repository created to store the source code of the challenge using the GraphQL Pokémon API and React.

## Build

```
> npm install
> npm start
```

## Technologies

* [React.js](https://github.com/facebook/react)